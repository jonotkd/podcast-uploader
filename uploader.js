function checkFileApiSupport () {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        return true;
    } else {
        return false;
    }
}

// function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Bytes';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

// create edit form after button click
function openEditForm (id) {
    $('table .btn-edit-episode').attr('disabled', true);
    $('table').wrap('<form id="edit-episode" action="/api/editepisode" method="post"></form>');
    var episode = $($('table tbody').children()[id]).children();
    var date = $(episode[0]).text();
    var title = $(episode[1]).text();
    var speaker = $(episode[2]).text();
    var duration = $(episode[3]).text();
    $(episode[0]).html('<input type="text" name="dateEdit" value="'+date+'">');
    $(episode[1]).html('<input type="text" name="titleEdit" value="'+title+'">');
    $(episode[2]).html('<input type="text" name="speakerEdit" value="'+speaker+'">');
    $(episode[3]).html('<input type="text" name="durationEdit" value="'+duration+'">');
    $(episode[4]).html('<input type="hidden" name="idEdit" value="'+id+'"><button type="submit" id="edit-submit" class="btn btn-xs btn-danger">save</button>');
}

$.fn.filterNode = function(name) {
    return this.find('*').filter(function() {
        return this.nodeName === name;
    });
};

$(document).ready(function () {

    fileApiSupport = checkFileApiSupport();
    if (!fileApiSupport) {
        alert('Your browser does not support the HTML5 File API. Please upgrade your browser!');
    }

    // Get episodes and write them to table
    itemList = new Array();
    data = $.get('Equippers_Berlin.rss', function (data) {
        $(data).find('item').each(function (i, item) {
            itemList[i] = new Object();
            itemList[i].title    = $(item).find('title').text();
            itemList[i].author   = $(item).find('itunes\\:author, author').text();
            itemList[i].url      = $(item).find('guid').text();
            itemList[i].pubDate  = $(item).find('pubDate').text();
            itemList[i].duration = $(item).find('itunes\\:duration, duration').text();

            // add to table
            $('table tbody').append(
                '<tr>' +
                '<td>' + itemList[i].pubDate + '</td>' +
                '<td>' + itemList[i].title + '</td>' +
                '<td>' + itemList[i].author + '</td>' +
                '<td>' + itemList[i].duration + '</td>' +
                '<td><a class="btn btn-xs btn-default btn-edit-episode" onclick="openEditForm('+i+');">edit</a></td>' +
                '</tr>'
            );
        });

        // get sunday and add 7 days and insert into date input
        sunday = new Date(itemList[0].pubDate);
        sunday.setDate(sunday.getDate()+7);
        year       = sunday.getFullYear();
        month      = sunday.getMonth()+1;
        day        = sunday.getDate();
        if (month < 10) { month = '0'+month; }
        if (day < 10) { day = '0'+day; }

        $('input[name=date]').val(year+'-'+month+'-'+day+' 12:30');
    });


    // get mp3 duration afer file selected
    $('input[name=file]').change(function (e) {
        if (fileApiSupport) {
            var freader = new FileReader();
            f = e.target.files[0];
            if (f.type == 'audio/mp3' || f.type == 'audio/mpeg') {
                freader.onload = function(evt) {
                    $('#player').attr('src', evt.target.result);
                    $('#player').on('canplay', function () {
                        duration = Math.floor($('#player')[0].duration);
                        hours = Math.floor(duration / 3600);
                        duration %= 3600;
                        minutes = Math.floor(duration / 60);
                        seconds = duration % 60;
                        if (hours == 0) { hours = '00'; } else if (hours < 10) {hours = '0'+hours; }
                        if (minutes == 0) { minutes = '00'; } else if (minutes < 10) {minutes = '0'+minutes; }
                        if (seconds == 0) { seconds = '00'; } else if (seconds < 10) {seconds = '0'+seconds; }

                        $('input[name=duration]').val(hours+':'+minutes+':'+seconds);
                    });
                };

                freader.readAsDataURL(f);
            } else {
                $('#output').html('Error: File does not seem to be mp3. Could not read duration.')
            }
        } else {
            $('#output').html('Could not read mp3 duration, cause your browser lakes support for the Javascript File API. Please upgrade your browser!');
            $('input[name=duration]').val('00:00:00');
        }
    });


    // upload functionality

    // check data before uploading
    function beforeSubmit () {
        if (fileApiSupport) {
            //check empty input fields
            if( !$('input[name=file]').val() ) {
                $('#output').html('Error: No file selected.');
                return false;
            }

            if ( $('input[name=date]').val() == '' ) {
                $('#output').html('Error: No date specified.');
                return false;
            }
            if ( $('input[name=title]').val() == '' ) {
                $('#output').html('Error: No title specified.');
                return false;
            }
            if ( $('input[name=speaker]').val() == '' ) {
                $('#output').html('Error: No speaker specified.');
                return false;
            }
            if ( $('input[name=duration]').val() == '' ) {
                $('#output').html('Error: No duration specified.');
                return false;
            }

            // get file type
            var ftype = $('input[name=file]')[0].files[0].type;
            // get file size
            var fsize = $('input[name=file]')[0].files[0].size;
            fsizeCalc = bytesToSize(fsize);

            // check file size
            if (fsize > 838860800) {
                $('#output').html('Error: File is too big ('+fsizeCalc+') Max file size is 100 MB.');
                return false;
            }

            // check if mp3
            switch(ftype) {
                case 'audio/mp3':
                    break;
                case 'audio/mpeg':
                    break;
                default:
                    $('#output').html('Error: File type is not mp3.');
                    return false;
            }

            $('#add-submit').attr('disabled', true); // disable submit button
            $('.progress').show(); // show progress bar
            $('#output').html(''); // reset output
        }
        else {
            $('#output').html('Please upgrade your browser, because your current browser lacks some new features we need!');
            return false;
        }
    }

    // get progress during upload
    function OnProgress (event, position, total, percentComplete) {
        $('.progress-bar').width(percentComplete+'%');
        $('.progress-bar').html(percentComplete+'%');
    }

    // after successful upload
    function afterSuccess (res) {
        if (res == 'success') {
            $('.progress').delay(1000).fadeOut(1000);
            $('#output').css('color', 'green');
            $('#output').html('Success! Episode added...');
            setTimeout(function () {
                window.location = './';
            },1000);
        } else {
            $('#output').html('error: '+res);
        }
    }
    
    function onError (res) {
        $('#output').html('error: '+res.responseText);
    }

    // jquery form options
    var options = {
        target:         '#output',    // target element(s) to be updated with server response
        beforeSubmit:   beforeSubmit, // pre-submit callback
        success:        afterSuccess, // post-submit callback
        uploadProgress: OnProgress,   // upload progress callback
        resetForm:      false,         // reset the form after successful submit
        error:          onError,
    };

    // add event listener
    $('#add-episode').submit(function() {
         // Todo: Check if this can somehow handle errors.
         $(this).ajaxSubmit(options);
         return false;
    });


    // edit form

    // check data before edit
    function beforeEdit () {
        //check empty input fields
        if ( $('input[name=dateEdit]').val() == '' ) {
            alert('Error: No date specified.');
            return false;
        }
        if ( $('input[name=titleEdit]').val() == '' ) {
            alert('Error: No title specified.');
            return false;
        }
        if ( $('input[name=speakerEdit]').val() == '' ) {
            alert('Error: No speaker specified.');
            return false;
        }
        if ( $('input[name=durationEdit]').val() == '' ) {
            alert('Error: No duration specified.');
            return false;
        }

        $('#edit-submit').attr('disabled', true);
    }

    // after successful edit
    function afterEdit (res) {
        if (res == 'success') {
            $('#edit-submit').removeClass('btn-danger').addClass('btn-success').append(' &#x2714;');
            setTimeout(function () {
                window.location = './';
            },1000);
        } else {
            alert('error: '+res);
        }
    }

    optionsEdit = {
        beforeSubmit:   beforeEdit, // pre-submit callback
        success:        afterEdit,  // post-submit callback
        resetForm:      false       // reset the form after successful submit
    };

    // add event listener
    $(document).on('submit', '#edit-episode', function() {
        $(this).ajaxSubmit(optionsEdit);
        return false;
    });
});
