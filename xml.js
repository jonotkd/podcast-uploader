// module specific to xml specific functionality.

var exports = module.exports = {};

var xml2js = require('xml2js');
var fs = require('fs');
var utils = require('./utils.js');

// Adds a new podcast.
exports.appendDataToXml = function (xmlstring, dic, cb) {
    xml2js.parseString(xmlstring, function (err, result) {
        var item = {};
        item.title = dic.title;
        item["itunes:author"] = dic.speaker;
        item["itunes:subtitle"] = dic.speaker;
        const url = "http://podcast.equippers.de/audio";
        item.guid = url + dic.file_name;
        item.enclosure = {};
        // $ is used for attributes.
        item.enclosure.$ = {};
        item.enclosure.$.length = 100;
        item.enclosure.$.type = "audio/mpeg";
        item.enclosure.$.url = url + dic.file_name;
        var date = dic.date.split(/[\-\ \:]/);
        var month = utils.getMonth(date[1]);
        var pubDate = 'Sun, ' + date[2] + ' ' + month + ' ' + date[0] + ' ' + date[3] + ':' + date[4] + ':00 +0100';
        item.pubDate = pubDate;
        item["itunes:explicit"] = "no";
        item["itunes:duration"] = dic.duration;
        result.rss.channel[0].item.unshift(item);
        var builder = new xml2js.Builder({ xmldec: { 'version': '1.0', 'encoding': 'UTF-8', 'standalone': false } });
        var xml = builder.buildObject(result);
        return cb(xml);
    });
}

// Edits the xml file using the parameters in the body and calls the callback.
exports.editExistingData = function (xmlstring, body, cb) {
    xml2js.parseString(xmlstring, function (err, result) {
        var id = parseInt(body.idEdit);
        console.dir(result.rss.channel[id]);
        var item = result.rss.channel[0].item[id];
        item.title[0] = body.titleEdit;
        item.pubDate[0] = body.dateEdit;
        item["itunes:duration"][0] = body.durationEdit;
        item["itunes:author"][0] = body.speakerEdit;
        item["itunes:subtitle"][0] = body.speakerEdit;
        var builder = new xml2js.Builder({ xmldec: { 'version': '1.0', 'encoding': 'UTF-8', 'standalone': false } });
        var xml = builder.buildObject(result);
        return cb(xml);
    });
}