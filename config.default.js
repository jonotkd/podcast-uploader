// configuration file for server-specific stuff
var config = module.exports = {};

config.serverPort = 3000; // port to listen for http connections
config.uploadDir = '/uploads/'; // directory that files willl be uploaded to
