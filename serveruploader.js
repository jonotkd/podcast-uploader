var express = require("express");
var Busboy = require('busboy'); //middleware for form/file upload
var done = false;
var bodyParser = require('body-parser');
var app = express();
var path = require('path');
var os = require('os');
var fs = require('fs');
var config = require('./config');
var xml = require('./xml.js');
var pug = require('pug');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/podcast');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var User = mongoose.model('User', new Schema( {
    id: ObjectId,
    name: { type: String, unique: true },
    password: String,
}));
var session = require( 'client-sessions' );
app.use( session({
    cookieName: "podcastsession",
    secret: "bobthebuilderisgreatforkids",
    // 1 hour
    duration: 60 * 60 * 1000,
    // If the above duration is at 59mins, then expand by 5 mins
    activeDuration: 5 * 60 * 1000,
    // Optional: When the browser is closed, kill the session,
    // disabled for now.
    // cookie:{
    //    ephemeral:true
    // }
}));
User.on('index',function(err){
    if(err) {
        console.log(err.message);
        //If this fails, try background:false next to unique:true
    }
});

app.set('view engine', 'pug');
app.get('/', function(req, res, error) {
    res.redirect('podcasts');
});
app.get('/login', function(req, res, error) {
    res.render('login');
});
app.get('/register', function(req, res, error) {
    res.render('register');
});
function loginRequired(req,res,next){
    if(!req.user){
        return res.redirect("/login");
    }
    next();
};
app.use(express.static(__dirname + "/"));

//Set the user object if the session Id is valid
//this is then checked by the function above (loginRequired)
app.use((req,res,next) => {
    if(!(req.podcastsession && req.podcastsession.userId)){
        return next();
    }

    User.findById(req.podcastsession.userId, (err,user)=>{
        if(err){
            return next(err);
        }
        if(!user){
            return next();
        }
        //Protect the password by removing it
        user.password = undefined;

        req.user = user;
        res.locals.user = user;

        next();
    });
});
app.get('/podcasts', loginRequired, function(req, res, error) {
    res.render('podcasts');
});
app.get('/logout', function(req, res, error) {
    // Clear session data, so that a relogin required
    req.podcastsession.reset();
    res.redirect('login');
});
//app.use(Busboy);       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

const saltRounds = 7;
const bcrypt = require('bcrypt');
app.post('/register', function(req, res) {
    var hash = bcrypt.hashSync(req.body.password,saltRounds);
    var user = new User({
        name: req.body.name,
        password: hash
    });
    user.save(function(err){
        if(err){
            var error = "Something bad happened, please try again or perhaps later.";
            if(err.code == 11000) {
                error = "That name is already taken, please try another";
            }
            res.render('register', {error:error});
        } else {
            res.render('register',{error:"Successfully created user"});
        }
    })
});
app.post('/login', function(req,res){
    User.findOne({name:req.body.name},function(err,user){
        if(!user || !bcrypt.compareSync(req.body.password,user.password) ) {
            res.render('login.pug', {error:"Incorrect name/password"});
        } else {
            // Save the 
            req.podcastsession.userId = user._id;
            res.redirect('podcasts');
        }
    });
});
app.route('/api/uploadaudio').post(function (req, res, next) {
    // 100Mb limit.
    var busboy = new Busboy({ headers: req.headers, limits: { fileSize: 838860800 } });
    var fields = 0;
    var dic = {};
    var mime_type;
    var limit_reached = false;

    busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        var directory = config.uploadDir;
        var save_to = path.join(directory, path.basename(filename));
        var save_to_final = path.join(__dirname, save_to);
        dic.file_name = save_to;
        mime_type = mimetype;
        console.log('saving ' + save_to + ' at full path:' + save_to_final);
        file.pipe(fs.createWriteStream(save_to_final));
        file.on('limit', function () {
            limit_reached = true;
            fs.unlinkSync(save_to_final);
        });
    });
    busboy.on('finish', function () {
        console.log('finished uploading, there are ' + fields + ' fields.');

        if (dic.date == "" || dic.title == "" || dic.speaker == "" || dic.duration == "") {
            res.writeHead(400);
            res.end("Not all data there");
            return;
        }

        switch (mime_type) {
            case 'audio/mpeg':
            case 'audio/mp3':
                break;
            default:
                res.end("wrong file type (not mp3)");
                return;
        }

        // check hash to ensure that only my form is used
        if (dic.hash == 'pfgdhwpzpjesx7wl') {
            if (limit_reached) {
                res.writeHead(400);
                res.end("Too big, size limit 100MB");
            }
            else {
                res.writeHead(200, { 'Connection': 'close' });
                res.end("success");
            }
        }
        else {
            res.writeHead(401);
            res.end("Wrong hash");
        }

        var rssFilename = "Equippers_Berlin.rss";
        fs.readFile(rssFilename, 'utf8', function (err, data) {
            if (err) throw err;
            xml.appendDataToXml(data, dic, function (xmlData) {
                fs.writeFile(rssFilename, xmlData, function (err) {
                    if (err) throw err;
                    console.log("Successfully added new item");
                });
            });
        });
    });
    busboy.on('field', function (fieldname, val, fieldnameTruncated, valTruncated) {
        console.log('Field [' + fieldname + ']: value: ' + val);
        dic[fieldname] = val;
    });
    return req.pipe(busboy);
});
app.route('/api/editepisode').post(function (req, res, next) {
    console.log(req.body);

    var rssFilename = "Equippers_Berlin.rss";
    fs.readFile(rssFilename, 'utf8', function (err, data) {
        xml.editExistingData(data, req.body, function (xmlData) {
            fs.writeFile(rssFilename, xmlData, (err) => {
                if (err) throw err;
                console.log("Success editing file.");
                res.writeHead(200, { 'Connection': 'close' });
                res.end("success");
            });
        });
    });
});
app.listen(config.serverPort, function () {
    console.log("Working on port 3000");
});
