# README #

the "podcast-uploader" lets users upload mp3 and mpeg audio files and allows editing of the xml file which is used to show which podcasts can be downloaded.

### How do I get set up? ###

* npm install
* rename config.default.js to config.js
* node serveruploader.js
* Open browser and direct to: http://localhost:3000/

### Database stuff ###

* run "sudo mongod&"
* Then run "mongo"
* "show dbs" → use one in the list, like "use podcasts" → show collections → db.users.find() z.B.


